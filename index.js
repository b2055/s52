function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if(letter.length != 1) {
        // If letter is invalid, return undefined.
        return undefined
    } else if (letter.length == 1) {
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        let stringSplit = sentence.split('')

        const iterate = stringSplit.map((character) => {
            if(letter === character) {
                result = result + 1
            }
        }).filter(notUndefined => notUndefined !== undefined)
    }

    return result
}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let textLowerCase = text.toLowerCase()
    let isogramCheck = textLowerCase.split("").every((c, i) => textLowerCase.indexOf(c) == i);
    return isogramCheck
}

function purchase(age, price) {
    
    if (age < 13) {
        // Return undefined for people aged below 13.
        return undefined
    } else if ((age >= 13 && age <= 21) || (age >= 65)) {
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        return ((price * 0.8).toFixed(2)).toString()
    } else {
        // Return the rounded off price for people aged 22 to 64.
        return (price.toFixed(2)).toString()
    }

    // The returned value should be a string.
    
}

function findHotCategories(items) {

    // Find categories that has no more stocks.
    let noMoreStocks = items.map((element) => {
        if(element.stocks == 0) {
            return element.category
        }
    })
    .filter(notUndefined => notUndefined !== undefined)
    .filter( function(element, index, inputArray ) {
        // The hot categories must be unique; no repeating categories.
        return inputArray.indexOf(element) == index;
    });
        
    return noMoreStocks
}

function findFlyingVoters(candidateA, candidateB) {
    
    // Find voters who voted for both candidate A and candidate B.
    const flyingVoter = candidateA.filter(element => candidateB.includes(element));
    return flyingVoter
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};